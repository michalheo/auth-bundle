<?php

namespace Heo\AuthenticationBundle\Security\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * A factory class used by Symfony Security Extension. It changes the DI configuration so additional
 * authentication providers can be configured in app/config/security.yml. This class exposes the api_token key
 * inside firewall configuration. There is no additional configuration possible.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenFactory implements SecurityFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'security.authentication.provider.api_token.'.$id;
        $container
            ->setDefinition($providerId, new DefinitionDecorator('heo_authentication.security.core.authentication.provider.api_token'))
        ;

        $listenerId = 'security.authentication.listener.api_token.'.$id;
        $container->setDefinition($listenerId, new DefinitionDecorator('heo_authentication.security.core.http.firewall.api_token_authentication_listener'));

        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    /**
     * {@inheritdoc}
     */
    public function getKey()
    {
        return 'api_token';
    }

    /**
     * {@inheritdoc}
     */
    public function addConfiguration(NodeDefinition $builder)
    {
    }
}
