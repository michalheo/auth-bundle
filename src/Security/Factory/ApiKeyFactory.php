<?php

namespace Heo\AuthenticationBundle\Security\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * A factory class used by Symfony Security Extension. It changes the DI configuration so additional
 * authentication providers can be configured in app/config/security.yml. This class exposes the api_key key
 * inside firewall configuration.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyFactory implements SecurityFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'security.authentication.provider.api_key.'.$id;
        $container
            ->setDefinition($providerId, new DefinitionDecorator('heo_authentication.security.core.authentication.provider.api_key'))
            ->replaceArgument(0, new Reference($userProvider))
        ;

        $listenerId = 'security.authentication.listener.api_key.'.$id;
        $container->setDefinition($listenerId, new DefinitionDecorator('heo_authentication.security.core.http.firewall.api_key_authentication_listener'));

        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    /**
     * {@inheritdoc}
     */
    public function getKey()
    {
        return 'api_key';
    }

    /**
     * {@inheritdoc}
     */
    public function addConfiguration(NodeDefinition $builder)
    {
        $builder
            ->children()
                ->scalarNode('provider')->defaultNull()
            ->end()
        ->end();
    }
}
