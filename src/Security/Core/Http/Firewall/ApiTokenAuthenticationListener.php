<?php

namespace Heo\AuthenticationBundle\Security\Core\Http\Firewall;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Heo\AuthenticationBundle\Security\Core\Authentication\Token\ApiToken;

/**
 * A firewall authentication listener that allows listen for X-Auth-ApiToken headers
 * and authenticate in the system.
 *
 * IMPORTANT!!
 * This class will return a HTTP:403 FORBIDDEN if the provided hash
 * will be invalid - can't be handled by any authentication provider.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenAuthenticationListener implements ListenerInterface
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var AuthenticationManagerInterface
     */
    protected $authenticationManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param TokenStorageInterface          $tokenStorage
     * @param AuthenticationManagerInterface $authenticationManager
     * @param LoggerInterface                $logger
     * @param EventDispatcherInterface       $dispatcher
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager, LoggerInterface $logger = null, EventDispatcherInterface $dispatcher = null)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->logger = $logger;
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        // We provide a consistent interface here, the legacy implementation
        // was looking for a header X-Auth-Token
        // In the current implementation we are looking for X-Auth-ApiToken
        $hash = $request->headers->get('X-Auth-Token', '');
        if ($request->headers->get('X-Auth-ApiToken', '')) {
            $hash = $request->headers->get('X-Auth-ApiToken', '');
        }

        if ($hash) {
            $token = new ApiToken();
            $token->setHash($hash);
            try {
                $authenticatedToken = $this->authenticationManager->authenticate($token);
                if ($authenticatedToken->isAuthenticated()) {
                    $this->tokenStorage->setToken($authenticatedToken);

                    return;
                }
            } catch (AuthenticationException $e) {
                $this->clearToken($e);
                $response = new Response();
                $response->setStatusCode(Response::HTTP_FORBIDDEN);
                $event->setResponse($response);
            }
        }

        return;
    }

    /**
     * Clears a PreAuthenticatedToken for this provider (if present).
     *
     * @param AuthenticationException $exception
     */
    private function clearToken(AuthenticationException $exception)
    {
        $token = $this->tokenStorage->getToken();
        if ($token instanceof ApiToken) {
            $this->tokenStorage->setToken(null);

            if (null !== $this->logger) {
                $this->logger->info('Cleared security token due to an exception.', array('exception' => $exception));
            }
        }
    }
}
