<?php

namespace Heo\AuthenticationBundle\Security\Core\Http\Firewall;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Response;
use Heo\AuthenticationBundle\Security\Core\Authentication\Token\ApiKeyToken;

/**
 * A firewall listener that allows to handle ApiKey headers
 * if they are specified.
 *
 * IMPORTANT!!
 * This class will return a HTTP:403 FORBIDDEN if the provided
 * ApiKey will be invalid - can't be handled by any authentication provider.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyAuthenticationListener implements ListenerInterface
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var AuthenticationManagerInterface
     */
    protected $authenticationManager;

    /**
     * @param TokenStorageInterface          $tokenStorage
     * @param AuthenticationManagerInterface $authenticationManager
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $apiKey = $request->headers->get('X-Auth-ApiKey', '');

        if ($apiKey) {
            $token = new ApiKeyToken();
            $token->setHash($apiKey);
            try {
                $authenticatedToken = $this->authenticationManager->authenticate($token);
                if ($authenticatedToken->isAuthenticated()) {
                    $this->tokenStorage->setToken($authenticatedToken);

                    return;
                }
            } catch (AuthenticationException $e) {
                $response = new Response();
                $response->setStatusCode(Response::HTTP_FORBIDDEN);
                $event->setResponse($response);
            }
        }

        return;
    }
}
