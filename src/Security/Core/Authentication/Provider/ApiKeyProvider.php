<?php

namespace Heo\AuthenticationBundle\Security\Core\Authentication\Provider;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Heo\AuthenticationBundle\Security\Core\Authentication\Token\ApiKeyToken;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * A Symfony authentication provider that allows to
 * use ApiKeys taken from the database.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyProvider implements AuthenticationProviderInterface
{
    /**
     * @var UserProviderInterface
     */
    protected $userProvider;

    /**
     * @param UserProviderInterface $userProvider
     */
    public function __construct(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(TokenInterface $token)
    {
        if (! $this->supports($token)) {
            return;
        }

        try {
            $user = $this->userProvider->loadUserByUsername($token->getHash());
            if ($user) {
                $authenticatedToken = new ApiKeyToken(array_merge(
                    $user->getRoles(),
                    array('ROLE_AUTHENTICATED_WITH_API_KEY')
                ));
                $authenticatedToken->setUser($user);
                $authenticatedToken->setAuthenticated(true);

                return $authenticatedToken;
            }
        } catch (UsernameNotFoundException $e) {
        }

        throw new AuthenticationException();
    }

    /**
     * {@inheritdoc}
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof ApiKeyToken;
    }
}
