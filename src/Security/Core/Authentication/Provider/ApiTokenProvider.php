<?php

namespace Heo\AuthenticationBundle\Security\Core\Authentication\Provider;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Heo\AuthenticationBundle\Service\ApiTokenService;
use Heo\AuthenticationBundle\Security\Core\Authentication\Token\ApiToken;

/**
 * A Symfony Authentication provider that allows use ApiTokens
 * stored inside the database.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenProvider implements AuthenticationProviderInterface
{
    /**
     * @var ApiTokenService
     */
    protected $apiTokenService;

    /**
     * @param ApiTokenService $apiTokenService
     */
    public function __construct(ApiTokenService $apiTokenService)
    {
        $this->apiTokenService = $apiTokenService;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(TokenInterface $token)
    {
        if (!$this->supports($token)) {
            return;
        }

        $hash = $token->getHash();
        $dbToken = $this->apiTokenService->matchToken($hash);

        if ($dbToken && $this->apiTokenService->isTokenValid($dbToken)) {
            $user = $dbToken->getUser();
            $authenticatedToken = new ApiToken(array('ROLE_AUTHENTICATED_WITH_API_TOKEN'));
            $authenticatedToken->setUser($user);
            $authenticatedToken->setHash($hash);
            $authenticatedToken->setAuthenticated(true);
            $this->apiTokenService->refreshToken($dbToken);

            return $authenticatedToken;
        }

        throw new AuthenticationException('The Api Token authentication failed.');
    }

    /**
     * {@inheritdoc}
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof ApiToken;
    }
}
