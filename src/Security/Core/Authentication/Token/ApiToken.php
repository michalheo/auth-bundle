<?php

namespace Heo\AuthenticationBundle\Security\Core\Authentication\Token;

use DateTime;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * A minimalistic class to store ApiTokens inside the session. We don't use
 * the ApiKey entity directly because it will cause problems with serialization
 * and handling Doctrine Proxy Classes.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiToken extends AbstractToken
{
    /**
     * @var string
     */
    protected $hash;

    /**
     * @var DateTime
     */
    protected $expires;

    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return DateTime
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @param DateTime $expires
     */
    public function setExpires(DateTime $expires)
    {
        $this->expires = $expires;
    }
}
