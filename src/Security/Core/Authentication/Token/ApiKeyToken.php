<?php

namespace Heo\AuthenticationBundle\Security\Core\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * A minimalistic class to store ApiKeyTokens inside the session. We don't use
 * the ApiKey entity directly because it will cause problems with serialization
 * and handling Doctrine Proxy Classes.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyToken extends AbstractToken
{
    /**
     * @var string
     */
    protected $hash;

    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
        return '';
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }
}
