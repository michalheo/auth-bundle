<?php

namespace Heo\AuthenticationBundle\Security\Core\User;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * A user provider used by symfony security layer.
 * It is used with combination of ApiKey. The ApiKey hash
 * is treaten like a username in this case. This class provides
 * the basic Symfony user class.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyUserProvider implements UserProviderInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @see UsernameNotFoundException
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        $repository = $this->getApiKeyRepository();
        $apiKey = $repository->findOneBy(array('hash' => $username));
        if ($apiKey && $apiKey->getDisabled() == false) {
            return new User($username, '', $apiKey->getRoles());
        }

        throw new UsernameNotFoundException();
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        if (! $user instanceof User) {
            throw new UnsupportedUserException();
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return User::class === $class;
    }

    /**
     * @return ObjectRepository
     */
    protected function getApiKeyRepository()
    {
        return $this->entityManager->getRepository('HeoAuthenticationBundle:ApiKey');
    }
}
