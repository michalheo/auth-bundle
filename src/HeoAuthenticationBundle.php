<?php

namespace Heo\AuthenticationBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Heo\AuthenticationBundle\Security\Factory\ApiKeyFactory;
use Heo\AuthenticationBundle\Security\Factory\ApiTokenFactory;

/**
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class HeoAuthenticationBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new ApiKeyFactory());
        $extension->addSecurityListenerFactory(new ApiTokenFactory());
    }
}
