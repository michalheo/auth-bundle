<?php

namespace Heo\AuthenticationBundle\Authentication;

use Heo\AuthenticationBundle\Security\Core\Authentication\Token\ApiKeyToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyChecker
{
    const DEFAULT_MESSAGE = 'Authentication with API Token required.';

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Checks if the currently authenticated user has a token of ApiKey type.
     *
     * @return bool
     */
    public function isAuthenticated()
    {
        return $this->tokenStorage->getToken() instanceof ApiKeyToken;
    }

    /**
     * Checks if the currently authenticated user has a token of ApiKey type and has a defined role.
     *
     * @param string $role
     *
     * @return bool
     */
    public function isAuthenticatedAndHasRole($role)
    {
        if ($this->isAuthenticated()) {
            $token = $this->tokenStorage->getToken();
            /** @var \Symfony\Component\Security\Core\Role\Role $tmpRole */
            foreach ($token->getRoles() as $tmpRole) {
                if ($tmpRole->getRole() == $role) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string $role    Role to be checked
     * @param string $message Optional message to be set when throwing an exception.
     *
     * @throws AccessDeniedException
     */
    public function requireApiKeyAuthenticationAndRole($role, $message = '')
    {
        if (!$this->isAuthenticatedAndHasRole($role)) {
            throw new AccessDeniedException($message ? $message : self::DEFAULT_MESSAGE);
        }
    }

    /**
     * @param string $message Optional message to be set when throwing an exception.
     *
     * @throws AccessDeniedException
     */
    public function requireApiKeyAuthentication($message = '')
    {
        if (!$this->isAuthenticated()) {
            throw new AccessDeniedException($message ? $message : self::DEFAULT_MESSAGE);
        }
    }
}
