<?php

namespace Heo\AuthenticationBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Heo\AuthenticationBundle\Entity\ApiToken;

/**
 *  A Service class to handle common ApiToken operations like:
 *  - creating by hash and user
 *  - refreshing
 *  - removing
 *  - searching
 *  - maintenance
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    protected $passwordEncoder;

    /**
     * FQN of the user entity
     *
     * @var string
     */
    protected $entityName;

    /**
     * Name of the field on the user entity used as username
     *
     * @var string
     */
    protected $entityUsernameField;

    /**
     * Name of the field on the user entity containing credentials
     *
     * @var string
     */
    protected $entityCredentialField;

    /**
     * Time in seconds of token validity
     *
     * @var int
     */
    protected $expires;

    /**
     * @param EntityManagerInterface       $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder       Service used to encode user password
     * @param string                       $entityName            FQN of the user entity
     * @param string                       $entityUsernameField   Name of the field on the user entity used as username
     * @param string                       $entityCredentialField Name of the field on the user entity containing credentials
     * @param int                          $expires               Time in seconds of token validity
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        $entityName,
        $entityUsernameField,
        $entityCredentialField,
        $expires
    ) {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityName = $entityName;
        $this->entityUsernameField = $entityUsernameField;
        $this->entityCredentialField = $entityCredentialField;
        $this->expires = $expires;
    }

    /**
     * Creates a new token for the user and returns the token
     *
     * @param object $user
     *
     * @return ApiToken
     */
    public function createToken($user)
    {
        $this->validateUserEntity($user);

        $newToken = new ApiToken();
        $newToken
            ->setUser($user)
            ->setHash($this->generateHash())
            ->setValid($this->generateExpirationDate())
        ;

        $this->entityManager->persist($newToken);
        $this->entityManager->flush();

        return $newToken;
    }

    /**
     * Refreshes the provided token by extending its expiration date
     *
     * @param ApiToken $token
     *
     * @return ApiToken The refreshed ApiToken
     */
    public function refreshToken(ApiToken $token)
    {
        $expDate = $this->generateExpirationDate();
        $token->setValid($expDate);

        /** @var ClassMetadataInfo $tokenClassMetadata */
        $tokenClassMetadata = $this->entityManager->getClassMetadata('Heo\AuthenticationBundle\Entity\ApiToken');

        $connection = $this->entityManager->getConnection();
        $connection->update(
            $tokenClassMetadata->getTableName(),
            array(
                $tokenClassMetadata->getColumnName('valid') => $expDate->format('Y-m-d H:i:s'),
            ),
            array(
                $tokenClassMetadata->getIdentifierColumnNames()[0] => $token->getId(),
            )
        );

        return $token;
    }

    /**
     * @param ApiToken $token
     */
    public function removeToken(ApiToken $token)
    {
        $this->entityManager->remove($token);
        $this->entityManager->flush();
    }

    /**
     * Searches the database for a token using the provided hash
     *
     * @param string $hash
     *
     * @return ApiToken|null Returns the token if the hash matches or null if not
     */
    public function matchToken($hash)
    {
        $repository = $this->getApiTokenRepository();
        $token = $repository->findOneBy(array('hash' => $hash));

        return $token ? $token : null;
    }

    /**
     * Searches the database for the user with provided username and password
     *
     * @param string $username    User username
     * @param string $credentials User password
     *
     * @return object|null Returns the matched user or null if the combination of username and credentials cannot be found
     */
    public function matchUser($username, $credentials = null)
    {
        $userRepository = $this->getUserRepository();
        $user = $userRepository->findOneBy(array($this->entityUsernameField => $username));

        if ($user) {
            if (! is_null($credentials)) {
                $encodedPassword = $this->encodePassword($user, $credentials);
                if ($user->{'get'.ucfirst($this->entityCredentialField)}() === $encodedPassword) {
                    return $user;
                }
            } else {
                return $user;
            }
        }

        return null;
    }

    /**
     * Checks if the provided token is valid
     *
     * @param ApiToken $token
     *
     * @return bool
     */
    public function isTokenValid(ApiToken $token)
    {
        return !$this->isTokenExpired($token);
    }

    /**
     * Checks if the provided token already expired
     *
     * @param ApiToken $token
     *
     * @return bool
     */
    public function isTokenExpired(ApiToken $token)
    {
        $tokenValid = $token->getValid();
        $datetime = new \DateTime();

        if ($tokenValid instanceof \DateTime) {
            if ($datetime->getTimestamp() <= $tokenValid->getTimestamp()) {
                return false;
            }
        }

        return true;
    }

    /**
     * This method removes expired tokens from the database
     *
     * @return array ApiToken[] Returns a collection of tokens that were removed
     */
    public function maintain()
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb
            ->select('t')
            ->from(ApiToken::class, 't')
            ->where('t.valid < :currentDateTime')
            ->setParameter('currentDateTime', new \DateTime('now'))
        ;

        $result = $qb->getQuery()->getResult();

        if (count($result)) {
            foreach ($result as $token) {
                $this->entityManager->remove($token);
            }
            $this->entityManager->flush();
        }

        return $result;
    }

    /**
     * Creates a token hash
     *
     * @return string
     */
    protected function generateHash()
    {
        return bin2hex(random_bytes(30));
    }

    /**
     * @param UserInterface $user
     * @param string        $plainPassword
     *
     * @return string
     */
    protected function encodePassword($user, $plainPassword)
    {
        return $this->passwordEncoder->encodePassword($user, $plainPassword);
    }

    /**
     * Generate a token expiration date
     *
     * @return \DateTime
     */
    protected function generateExpirationDate()
    {
        $dateTime = new \DateTime();
        $dateTime->add(new \DateInterval('PT'.$this->expires.'S'));

        return $dateTime;
    }

    /**
     * @return ObjectRepository
     */
    protected function getApiTokenRepository()
    {
        return $this->entityManager->getRepository(ApiToken::class);
    }

    /**
     * @return ObjectRepository
     */
    protected function getUserRepository()
    {
        return $this->entityManager->getRepository($this->entityName);
    }

    /**
     * @param object $user
     *
     * @throws \InvalidArgumentException
     */
    protected function validateUserEntity($user)
    {
        if (!is_object($user)) {
            throw new \InvalidArgumentException('Provided user argument must be an object, '.gettype($user).' given');
        }

        $entityClass = $this->entityName;
        if (!$user instanceof $entityClass) {
            $providedUserClass = get_class($user);
            throw new \InvalidArgumentException("Provided user argument must be an instance of $entityClass. Instance of $providedUserClass given");
        }
    }
}
