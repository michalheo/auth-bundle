<?php

namespace Heo\AuthenticationBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Security\Core\Role\Role;
use Heo\AuthenticationBundle\Entity\ApiKey;

/**
 * A Service class to handle common ApiKey operations like:
 * - creating new ApiKeys in the database
 * - searching for ApiKeys in the database
 * - listing ApiKeys from the database
 * - updating ApiKeys in the database
 * - removing ApiKeys from the database
 * - disabling ApiKeys
 * - enabling ApiKeys
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array  $roles
     * @param string $hash
     *
     * @return ApiKey
     */
    public function createApiKey(array $roles, $hash = null)
    {
        if (!$hash) {
            $hash = $this->generateHash();
        }

        if ($this->findApiKey($hash)) {
            throw new \InvalidArgumentException('Api key with provided hash already exists.');
        }

        $roles = array_unique($roles);
        $this->validateRoles($roles);

        $apiKey = new ApiKey();
        $apiKey->setHash($hash);
        $apiKey->setRoles($roles);

        $this->entityManager->persist($apiKey);
        $this->entityManager->flush();

        return $apiKey;
    }

    /**
     * @param string $hash
     *
     * @return ApiKey|null
     */
    public function findApiKey($hash)
    {
        $repository = $this->entityManager->getRepository(ApiKey::class);
        $apiKey = $repository->findOneBy(array('hash' => $hash));

        return $apiKey ? $apiKey : null;
    }

    /**
     * @return array
     */
    public function listApiKeys()
    {
        return $this->entityManager->getRepository(ApiKey::class)->findAll();
    }

    /**
     * @param string $hash
     * @param array  $roles
     *
     * @return ApiKey
     */
    public function updateApiKey($hash, array $roles)
    {
        $apiKey = $this->findApiKey($hash);

        if ($apiKey) {
            $roles = array_unique($roles);
            $this->validateRoles($roles);
            $apiKey->setRoles($roles);
            $this->entityManager->persist($apiKey);
            $this->entityManager->flush();

            return $apiKey;
        }

        $this->throwApiKeyNotFoundException($hash);
    }

    /**
     * @param string $hash
     */
    public function removeApiKey($hash)
    {
        $apiKey = $this->findApiKey($hash);

        if ($apiKey) {
            $this->entityManager->remove($apiKey);
            $this->entityManager->flush();

            return;
        }

        $this->throwApiKeyNotFoundException($hash);
    }

    /**
     * @param string $hash
     *
     * @return ApiKey
     */
    public function disableApiKey($hash)
    {
        $apiKey = $this->findApiKey($hash);

        if ($apiKey) {
            $apiKey->setDisabled(true);
            $this->entityManager->persist($apiKey);
            $this->entityManager->flush();

            return $apiKey;
        }

        $this->throwApiKeyNotFoundException($hash);
    }

    /**
     * @param string $hash
     *
     * @return ApiKey
     */
    public function enableApiKey($hash)
    {
        $apiKey = $this->findApiKey($hash);

        if ($apiKey) {
            $apiKey->setDisabled(false);
            $this->entityManager->persist($apiKey);
            $this->entityManager->flush();

            return $apiKey;
        }

        $this->throwApiKeyNotFoundException($hash);
    }

    /**
     * @param array $roles An array of Roles or strings in the following format: ROLE_*
     *
     * @throws \InvalidArgumentException
     */
    protected function validateRoles(array $roles)
    {
        if (!count($roles)) {
            throw new \InvalidArgumentException('You must provide at least one role');
        }
        foreach ($roles as $role) {
            if ($role instanceof Role) {
                continue;
            } elseif (is_string($role)) {
                if (!preg_match('/^ROLE_[A-Z0-9]+$/', $role)) {
                    throw new \InvalidArgumentException('Invalid role name format. Format ^ROLE_[A-Z0-9]+$ expected');
                }
            }
        }
    }

    /**
     * @return string
     */
    protected function generateHash()
    {
        return bin2hex(random_bytes(45));
    }

    /**
     * @param string $hash
     *
     * @throws EntityNotFoundException
     */
    private function throwApiKeyNotFoundException($hash)
    {
        throw new EntityNotFoundException("ApiKey with hash $hash cannot be found in the database");
    }
}
