<?php

namespace Heo\AuthenticationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * An entity representing ApiKeys that are used for authentication.
 * ApiKeys should be used to authenticate users or applications that
 * are trusted regarding lack of password. The hash value is the identity
 * and credentials at the same time. Also roles can be defined and used later
 * in your APP.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 *
 * @ORM\Entity
 * @ORM\Table(name="authentication_api_key")
 */
class ApiKey
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $hash;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $disabled = false;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    protected $roles = array();

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return ApiKey
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     *
     * @return ApiKey
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     *
     * @return ApiKey
     */
    public function setRoles(array $roles = array())
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return bool
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * @param bool $disabled
     *
     * @return ApiKey
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }
}
