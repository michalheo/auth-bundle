<?php

namespace Heo\AuthenticationBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * An entity class that represents ApiTokens in the database.
 * ApiTokens are connected to a user entity in you application.
 * User class configuration should be defined in your app configuration under
 * heo_authentication.api_token.entity key.
 * Basically ApiTokens are valid for some period of time, for example 24h.
 * If there are no requests performed to the application that use a particular
 * ApiToken, it will expire. An expired ApiToken can be refreshed using a console
 * command or removed by an maintenance command. Expired ApiTokens cannot be used
 * to authenticate in the system.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 *
 * @ORM\Entity
 * @ORM\Table(name="authentication_api_token")
 */
class ApiToken
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    protected $hash;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updated;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $valid;

    /**
     * The mapping is being added via event dispatcher
     *
     * @var UserInterface
     */
    protected $user;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return ApiToken
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     *
     * @return ApiToken
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * @param DateTime $valid
     *
     * @return ApiToken
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     *
     * @return ApiToken
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     *
     * @return ApiToken
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param DateTime $updated
     *
     * @return ApiToken
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }
}
