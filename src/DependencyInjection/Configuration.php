<?php

namespace Heo\AuthenticationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('heo_authentication');

        $rootNode
            ->children()
                ->arrayNode('api_token')
                    ->isRequired()
                    ->children()
                        ->integerNode('expires')
                            ->info('This value defines the token expiration time.')
                            ->min(1)
                            ->defaultValue(86400) // default ApiToken expiration date - 24h
                        ->end()
                        ->arrayNode('entity')
                            ->isRequired()
                            ->children()
                                ->scalarNode('class')
                                    ->info('Fill this node with the FQN of the entity class that you are using in your project.')
                                    ->cannotBeEmpty()
                                    ->isRequired()
                                ->end()
                                ->scalarNode('identifier_field')
                                    ->info('Fill this node with the column name that is used as identifier in you user entity class')
                                    ->cannotBeEmpty()
                                    ->isRequired()
                                ->end()
                                ->scalarNode('username_field')
                                    ->info('Fill this node with the column name that is used as username in you user entity class')
                                    ->cannotBeEmpty()
                                    ->isRequired()
                                ->end()
                                ->scalarNode('credential_field')
                                    ->info('Fill this node with the column name that is used as credentials in you user entity class')
                                    ->cannotBeEmpty()
                                    ->isRequired()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
