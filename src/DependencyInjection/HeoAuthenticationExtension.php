<?php

namespace Heo\AuthenticationBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class HeoAuthenticationExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $this->exposeParameters($container, $config);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * @param ContainerBuilder $container
     * @param array            $config
     */
    protected function exposeParameters(ContainerBuilder $container, $config)
    {
        $container->setParameter('heo_authentication.api_token.entity.class', $config['api_token']['entity']['class']);
        $container->setParameter('heo_authentication.api_token.entity.identifier_field', $config['api_token']['entity']['identifier_field']);
        $container->setParameter('heo_authentication.api_token.entity.username_field', $config['api_token']['entity']['username_field']);
        $container->setParameter('heo_authentication.api_token.entity.credential_field', $config['api_token']['entity']['credential_field']);
        $container->setParameter('heo_authentication.api_token.expires', $config['api_token']['expires']);
    }
}
