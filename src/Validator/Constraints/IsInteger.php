<?php

namespace Heo\AuthenticationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class IsInteger extends Constraint
{
    public $message = 'This value should be a valid integer.';
}
