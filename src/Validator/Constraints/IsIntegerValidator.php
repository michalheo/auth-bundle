<?php

namespace Heo\AuthenticationBundle\Validator\Constraints;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class IsNumberValidator
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class IsIntegerValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof IsInteger) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\IsInteger');
        }

        if (is_int($value) || (is_numeric($value) && preg_match('/^\d+$/', $value))) {
            return;
        }

        if ($this->context instanceof ExecutionContextInterface) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->addViolation();
        } else {
            $this->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->addViolation();
        }
    }
}
