<?php

namespace Heo\AuthenticationBundle\Controller;

use DateTime;
use Heo\AuthenticationBundle\Service\ApiTokenService;
use Heo\AuthenticationBundle\Validator\Constraints\IsInteger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * A controller for handling login and logout regarding ApiTokens.
 * Contains two actions:
 * - loginAction
 * - logoutAction
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenController extends Controller
{
    /**
     * Authenticates user using username and password. If authentication is successful
     * returns a token and its expiration date as JSON.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function loginAction(Request $request)
    {
        $form = $this->createLoginForm();
        $form->handleRequest($request);

        $errors = array();
        $ret = null;
        try {
            if ($form->isValid()) {
                $apiTokenService = $this->getApiTokenService();
                $data = $form->getData();
                $username = $data['username'];
                $password = $data['password'];
                if ($user = $apiTokenService->matchUser($username, $password)) {
                    $authToken = $apiTokenService->createToken($user);
                    $ret = json_encode(array(
                        'username' => $username,
                        'token' => $authToken->getHash(),
                        'expires' => $authToken->getValid()->format(DateTime::ISO8601),
                    ));
                } else {
                    $errors[] = 'User not found';
                }
            } else {
                if ($form->isSubmitted()) {
                    $err = iterator_to_array($form->getErrors(true, true));
                    foreach ($err as $error) {
                        $errors[] = $error->getMessage();
                    }
                } else {
                    $errors[] = 'No data was submitted';
                }
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }

        $response = new Response(count($errors) ? json_encode(array('errors' => $errors)) : $ret, count($errors) ? Response::HTTP_UNAUTHORIZED : Response::HTTP_OK);
        $response->headers->add(array('Content-Type' => 'application/json'));

        return $response;
    }

    /**
     * This action removes the ApiToken used for the authentication from the database.
     * There is no need to provide any additional data using POST because you are passing
     * the hash value inside a X-Auth-Token header so the application can recognize your
     * identity.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function logoutAction(Request $request)
    {
        // We provide a consistent interface here, the legacy implementation
        // was looking for a header X-Auth-Token
        // In the current implementation we are looking for X-Auth-ApiToken
        $hash = $request->headers->get('X-Auth-Token', '');
        if ($request->headers->get('X-Auth-ApiToken', '')) {
            $hash = $request->headers->get('X-Auth-ApiToken', '');
        }

        $apiTokenService = $this->getApiTokenService();
        $apiToken = $apiTokenService->matchToken($hash);

        if (!$apiToken) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        $apiTokenService->removeToken($apiToken);

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * Create a Symfony form containing username and password fields.
     * CSRF protection needs to be disabled.
     *
     * @return FormInterface
     */
    protected function createLoginForm()
    {
        /* @var $factory \Symfony\Component\Form\FormFactory */
        $factory = $this->get('form.factory');
        $form = $factory->createNamedBuilder('login', 'form', array(), array('csrf_protection' => false))
            ->add('username', 'text', array(
                'required' => true,
                'constraints' => array(
                    new IsInteger(),
                    new NotNull(array('message' => 'Username cannot be empty')),
                    new Length(array('min' => 1)),
                ),
            ))
            ->add('password', 'text', array(
                'required' => true,
                'constraints' => array(
                    new NotNull(array('message' => 'Password cannot be empty')),
                    new Length(array('min' => 1)),
                ),
            ))
            ->getForm()
        ;

        return $form;
    }

    /**
     * @return ApiTokenService
     */
    protected function getApiTokenService()
    {
        return $this->container->get('heo_authentication.service.api_token_service');
    }
}
