<?php

namespace Heo\AuthenticationBundle\Listener;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Heo\AuthenticationBundle\Entity\ApiToken;

/**
 * A class that allows to move the definition of applicaiton
 * user entity from annotation inside Entity/ApiToken.php to the
 * configuration. Thanks to this idea it is possible to modify the
 * config without forking or patching the bundle.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class MappingListener
{
    /**
     * Contains the entity name of a user entity that should be connected with the ApiKey.
     * For example: FooBarBundle:UserEntity
     *
     * @var string
     */
    protected $targetEntityName;

    /**
     * Contains the name of the database column used as an identifier
     * in the application user entity.
     *
     * @var string
     */
    protected $targetEntityIdentifier;

    /**
     * @param string $targetEntityName
     * @param string $targetEntityIdentifier
     */
    public function __construct($targetEntityName, $targetEntityIdentifier)
    {
        $this->targetEntityName = $targetEntityName;
        $this->targetEntityIdentifier = $targetEntityIdentifier;
    }

    /**
     * Dynamically update Doctrine configuration.
     *
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $classMetadata = $eventArgs->getClassMetadata();
        if ($classMetadata->getName() == ApiToken::class) {
            $classMetadata->mapManyToOne(array(
                'fieldName' => 'user',
                'targetEntity' => $this->targetEntityName,
                'joinColumn' => array(
                    'name' => 'user_id',
                    'referencedColumnName' => $this->targetEntityIdentifier,
                    'onDelete' => 'CASCADE',
                ),
                'joinColumns' => array(
                    0 => array(
                        'onDelete' => 'CASCADE',
                    ),
                ),
            ));
        }
    }
}
