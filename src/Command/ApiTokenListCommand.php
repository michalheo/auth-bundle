<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Heo\AuthenticationBundle\Entity\ApiToken;

/**
 * A command for listing all ApiTokens available for a user.
 * It will list expired and non-expired tokens. You need to provide
 * a valid username, it is not possible to display all ApiTokens present
 * in the database.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenListCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-token:list')
            ->setDescription('Lists tokens of provided user')
            ->addArgument('username', InputArgument::REQUIRED, 'The user username', null)
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $tokens = $this->findTokensByUsername($username);

        if (count($tokens)) {
            $output->writeln('Tokens for user '.$username.':');
            /* @var $apiTokenService \Heo\AuthenticationBundle\Service\ApiTokenService */
            $apiTokenService = $this->getContainer()->get('heo_authentication.service.api_token_service');
            /* @var $token ApiToken */
            foreach ($tokens as $token) {
                $outputLine = $token->getId()."\t".$token->getHash()."\t".$token->getValid()->format(\DateTime::ISO8601);
                if ($apiTokenService->isTokenValid($token)) {
                    $output->writeln($outputLine);
                } else {
                    $output->writeln('<error>'.$outputLine.'</error>');
                }
            }
        }
    }

    /**
     * @param string $username
     *
     * @return object
     */
    protected function findTokensByUsername($username)
    {
        $container = $this->getContainer();
        $container->getParameter('heo_authentication.api_token.entity.class');
        $entityUsernameField = $container->getParameter('heo_authentication.api_token.entity.username_field');

        /* @var $entityManager \Doctrine\ORM\EntityManagerInterface */
        $entityManager = $container->get('doctrine.orm.entity_manager');
        $qb = $entityManager->createQueryBuilder();
        $qb
            ->select('t')
            ->from(ApiToken::class, 't')
            ->leftJoin('t.user', 'u')
            ->where('u.'.$entityUsernameField.' = :username')
            ->orderBy('t.valid', 'DESC')
            ->setParameter('username', $username)
        ;

        return $qb->getQuery()->getResult();
    }
}
