<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A command that allows you to enable a disabled ApiKey
 * that is stored in the database.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyEnableCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-key:enable')
            ->setDescription('Enables an ApiKey in the database.')
            ->addArgument('hash', InputArgument::REQUIRED, 'The ApiKey hash', null)
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hash = $input->getArgument('hash');

        /* @var $apiKeyService \Heo\AuthenticationBundle\Service\ApiKeyService */
        $apiKeyService = $this->getContainer()->get('heo_authentication.service.api_key_service');

        try {
            $apiKeyService->enableApiKey($hash);
            $output->writeln("Api key with hash $hash enabled");
        } catch (\Exception $e) {
            $output->writeln('<error>'.$e->getMessage().'</error>');
        }
    }
}
