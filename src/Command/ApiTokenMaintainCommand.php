<?php
namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Heo\AuthenticationBundle\Entity\ApiToken;

/**
 * A command that should be used to cleanup the database
 * from expired ApiTokens. Those tokens will be removed permanently.
 * Use this command manually or / and setup a CRON job to perform
 * a cleanup.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenMaintainCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-token:maintain')
            ->setDescription('Cleans up expired tokens from the database')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $apiTokenService = $this->getContainer()->get('heo_authentication.service.api_token_service');
        $removedTokens = $apiTokenService->maintain();

        if (count($removedTokens)) {
            if ($output->getVerbosity() == OutputInterface::VERBOSITY_VERBOSE) {
                $this->outputVerbose($removedTokens, $output);
            } else {
                $this->outputNormal($removedTokens, $output);
            }
        } else {
            $output->writeln('<info>Database is clean. Nothing to remove</info>');
        }
    }

    /**
     * @param Collection      $removedTokens ApiToken[] Removed ApiTokens
     * @param OutputInterface $output
     */
    protected function outputVerbose($removedTokens, OutputInterface $output)
    {
        $output->writeln('<info>Removed following api tokens:</info>');
        foreach ($removedTokens as $token) {
            $output->writeln($token->getHash()."\t<error>".$token->getValid()->format(\DateTime::ISO8601)."</error>");
        }
    }

    /**
     * @param Collection      $removedTokens ApiToken[] Removed ApiTokens
     * @param OutputInterface $output
     */
    protected function outputNormal($removedTokens, OutputInterface $output)
    {
        $output->writeln('<info>Removed '.count($removedTokens).' invalid api tokens from the database</info>');
    }
}
