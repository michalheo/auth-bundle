<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Heo\AuthenticationBundle\Entity\ApiToken;

/**
 * A command that allows to perform the same operation as when using HTTP request -
 * create an ApiToken and "login" into the system.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenVirtualLoginCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-token:virtual-login')
            ->setDescription('Allows to perform the same operation as in the frontend but using the console. Useful for debugging')
            ->addArgument('username', InputArgument::REQUIRED, 'The user username', null)
            ->addArgument('password', InputArgument::REQUIRED, 'The user password', null)
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');

        $apiTokenService = $this->getContainer()->get('heo_authentication.service.api_token_service');
        $user = $apiTokenService->matchUser($username, $password);

        if (!$user) {
            $output->writeln('<error>User not found for provided credentials</error>');
        } else {
            $token = $apiTokenService->createToken($user);

            $output->writeln('Logged in and created token:');
            $output->writeln("\t username: ".$username);
            $output->writeln("\t token id: ".$token->getId());
            $output->writeln("\t token hash: ".$token->getHash());
            $output->writeln("\t token expires: ".$token->getValid()->format(\Datetime::ISO8601));
        }
    }
}
