<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A command for removing unused / unwanted ApiKeys from the database.
 * Only ApiKeys stored in tables created by this bundle will be affected.
 * The ApiKeys are removed permanently so be careful of what you are doing!
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyRemoveCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-key:remove')
            ->setDescription('Removes an ApiKey from the database')
            ->addArgument('hash', InputArgument::REQUIRED, 'The ApiKey hash', null)
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hash = $input->getArgument('hash');

        /* @var $apiKeyService \Heo\AuthenticationBundle\Service\ApiKeyService */
        $apiKeyService = $this->getContainer()->get('heo_authentication.service.api_key_service');

        try {
            $apiKeyService->removeApiKey($hash);
            $output->writeln('Remove ApiKey with hash '.$hash);
        } catch (\Exception $e) {
            $output->writeln('<error>'.$e->getMessage().'</error>');
        }
    }
}
