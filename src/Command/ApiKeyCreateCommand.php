<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A Symfony console command for creating ApiKeys.
 * The command allows to define the roles assigned to an ApiKey,
 * you can also specify the hash value that should be saved in
 * the database.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyCreateCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-key:create')
            ->setDescription('A command for creating a new ApiKey')
            ->addArgument('roles', InputArgument::IS_ARRAY, 'Roles assigned to the Api Key', null)
            ->addOption('hash', null, InputOption::VALUE_REQUIRED, 'If you need to set a specific hash, please set it here', null)
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $roles = $input->getArgument('roles');
        $hash = $input->getOption('hash');

        /* @var $apiKeyService \Heo\AuthenticationBundle\Service\ApiKeyService */
        $apiKeyService = $this->getContainer()->get('heo_authentication.service.api_key_service');
        $apiKey = $apiKeyService->createApiKey($roles, $hash);

        $output->writeln('Created ApiKey with the following values:');
        $output->writeln('Hash: '.$apiKey->getHash());
        $output->writeln('Roles: '.implode(', ', $apiKey->getRoles()));
    }
}
