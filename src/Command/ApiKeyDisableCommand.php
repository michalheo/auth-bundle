<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A command that temporarily disables an ApiKey available
 * in the database. This key cannot be used in requests, the user
 * won't be authenticated but the key still persists in the database
 * so it can be re-enabled again.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyDisableCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-key:disable')
            ->setDescription('Disables an ApiKey in the database.')
            ->addArgument('hash', InputArgument::REQUIRED, 'The ApiKey hash', null)
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hash = $input->getArgument('hash');

        /* @var $apiKeyService \Heo\AuthenticationBundle\Service\ApiKeyService */
        $apiKeyService = $this->getContainer()->get('heo_authentication.service.api_key_service');

        try {
            $apiKeyService->disableApiKey($hash);
            $output->writeln("Api key with hash $hash disabled");
        } catch (\Exception $e) {
            $output->writeln('<error>'.$e->getMessage().'</error>');
        }
    }
}
