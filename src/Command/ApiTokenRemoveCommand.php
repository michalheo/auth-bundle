<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Heo\AuthenticationBundle\Entity\ApiToken;

/**
 * Allows to permanently remove an ApiToken from the database.
 * You need to provide a valid hash. Please be aware that this operation
 * can cause unwanted behaviour if a user is currently using this token somewhere.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenRemoveCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-token:remove')
            ->setDescription('Allows to remove a token from the database')
            ->addArgument('hash', InputArgument::REQUIRED, 'Unique hash of the token', null)
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hash = $input->getArgument('hash');
        $token = $this->findTokenByHash($hash);

        if (!$token) {
            throw new \InvalidArgumentException('Token for hash '.$hash.' cannot be found');
        }

        /* @var $apiTokenService \Heo\AuthenticationBundle\Service\ApiTokenService */
        $apiTokenService = $this->getContainer()->get('heo_authentication.service.api_token_service');
        $apiTokenService->removeToken($token);

        $output->writeln('<info>Removed token with hash: '.$token->getHash().'</info>');
    }

    /**
     * @param string $hash
     *
     * @return ApiToken|null
     */
    protected function findTokenByHash($hash)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $token = $em->getRepository(ApiToken::class)->findOneBy(array('hash' => $hash));

        return $token ? $token : null;
    }
}
