<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A command for updating the ApiKey. You are able to change the roles
 * that should be assigned. Please be aware that this applies only to keys
 * stored in the database.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyUpdateCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-key:update')
            ->setDescription('Allows to update an ApiKey and set different roles.')
            ->addArgument('hash', InputArgument::REQUIRED, 'The ApiKey hash', null)
            ->addArgument('roles', InputArgument::IS_ARRAY, 'Roles assigned to the Api Key', null)
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hash = $input->getArgument('hash');
        $roles = $input->getArgument('roles');

        /* @var $apiKeyService \Heo\AuthenticationBundle\Service\ApiKeyService */
        $apiKeyService = $this->getContainer()->get('heo_authentication.service.api_key_service');

        try {
            $apiKey = $apiKeyService->updateApiKey($hash, $roles);
            $output->writeln('Updated ApiKey with the following values:');
            $output->writeln('Hash: '.$apiKey->getHash());
            $output->writeln('Roles: '.implode(', ', $apiKey->getRoles()));
        } catch (\Exception $e) {
            $output->writeln('<error>'.$e->getMessage().'</error>');
        }
    }
}
