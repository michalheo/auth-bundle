<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Heo\AuthenticationBundle\Entity\ApiToken;

/**
 * A command that allows to refresh an ApiToken and extend its
 * expiration date-time. Also expired tokens can be handled if they
 * are still present in the database.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenRefreshCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-token:refresh')
            ->setDescription('Allows to refresh a token and extend its validity')
            ->addArgument('hash', InputArgument::REQUIRED, 'Unique hash of the token', null)
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hash = $input->getArgument('hash');
        $token = $this->findTokenByHash($hash);

        if (!$token) {
            throw new \InvalidArgumentException('Token for hash '.$hash.' cannot be found');
        }

        /* @var $apiTokenService \Heo\AuthenticationBundle\Service\ApiTokenService */
        $apiTokenService = $this->getContainer()->get('heo_authentication.service.api_token_service');
        $previousValidDate = clone $token->getValid();
        $wasTokenValidBefore = $apiTokenService->isTokenValid($token);

        $apiTokenService->refreshToken($token);

        $output->writeln('Refreshed token with hash '.$token->getHash());
        if ($wasTokenValidBefore) {
            $output->writeln("valid before:\t".$previousValidDate->format(\DateTime::ISO8601));
        } else {
            $output->writeln("<error>valid before:\t".$previousValidDate->format(\DateTime::ISO8601).'</error>');
        }
        $output->writeln("valid after: \t".$token->getValid()->format(\DateTime::ISO8601));
    }

    /**
     * @param string $hash
     *
     * @return ApiToken|null
     */
    protected function findTokenByHash($hash)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $token = $em->getRepository(ApiToken::class)->findOneBy(array('hash' => $hash));

        return $token ? $token : null;
    }
}
