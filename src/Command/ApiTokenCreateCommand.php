<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Heo\AuthenticationBundle\Entity\ApiToken;

/**
 * A command for creating a ApiToken using the console.
 * Mostly you will use it when you don't a know a users password
 * but you need to use his credentials to perform some tests.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiTokenCreateCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-token:create')
            ->setDescription('Allows to create a new ApiToken for a user, using only his username')
            ->addArgument('username', InputArgument::REQUIRED, 'The user username', null)
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $user = $this->findUserByUsername($username);

        if (!$user) {
            throw new \InvalidArgumentException('Invalid username provided');
        }

        /* @var $apiTokenService \Heo\AuthenticationBundle\Service\ApiTokenService */
        $apiTokenService = $this->getContainer()->get('heo_authentication.service.api_token_service');
        $token = $apiTokenService->createToken($user);

        $container = $this->getContainer();
        $entityClass = $container->getParameter('heo_authentication.api_token.entity.class');
        $entityIdentifierField = $container->getParameter('heo_authentication.api_token.entity.username_field');
        $entityUsernameField = $container->getParameter('heo_authentication.api_token.entity.username_field');

        $output->writeln('Generated token for user of class '.$entityClass);
        $output->writeln("\t user id: ".$this->getUserFieldValue($token, $entityIdentifierField));
        $output->writeln("\t username: ".$this->getUserFieldValue($token, $entityUsernameField));
        $output->writeln("\t token id: ".$token->getId());
        $output->writeln("\t token hash: ".$token->getHash());
        $output->writeln("\t token expires: ".$token->getValid()->format(\Datetime::ISO8601));
    }

    /**
     * @param string $username
     *
     * @return object
     */
    protected function findUserByUsername($username)
    {
        $container = $this->getContainer();
        $entityClass = $container->getParameter('heo_authentication.api_token.entity.class');
        $entityUsernameField = $container->getParameter('heo_authentication.api_token.entity.username_field');

        $entityManager = $container->get('doctrine.orm.entity_manager');
        $repository = $entityManager->getRepository($entityClass);

        return $repository->findOneBy(array(
            $entityUsernameField => $username,
        ));
    }

    /**
     * @param ApiToken $token
     * @param string   $fieldName User fieldname
     *
     * @return string
     */
    protected function getUserFieldValue(ApiToken $token, $fieldName)
    {
        return $token->getUser()->{'get'.ucfirst($fieldName)}();
    }
}
