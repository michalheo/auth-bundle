<?php

namespace Heo\AuthenticationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A command for listing all available ApiKeys in the database.
 * This applies only to ApiKeys that are stored in tables created by
 * the bundle. It is possible to craft ApiKeys using a different provider
 * defined in app/config/security.yml but those keys will NOT be listed here.
 *
 * @author Michał Hepner <michal.hepner@gmail.com>
 */
class ApiKeyListCommand extends ContainerAwareCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('authentication:api-key:list')
            ->setDescription('Lists available api keys')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $apiKeyService \Heo\AuthenticationBundle\Service\ApiKeyService */
        $apiKeyService = $this->getContainer()->get('heo_authentication.service.api_key_service');
        $apiKeys = $apiKeyService->listApiKeys();

        $colLengths = array(
            'disabled' => strlen('disabled'),
            'hash' => 0,
            'roles' => 0,
        );

        foreach ($apiKeys as $apiKey) {
            $hashStrlen = strlen($apiKey->getHash());
            if ($hashStrlen > $colLengths['hash']) {
                $colLengths['hash'] = $hashStrlen;
            }

            $rolesStrlen = strlen(implode(', ', $apiKey->getRoles()));
            if ($rolesStrlen > $colLengths['roles']) {
                $colLengths['roles'] = $rolesStrlen;
            }
        }

        foreach ($apiKeys as $apiKey) {
            $string = '';
            if ($apiKey->getDisabled()) {
                $string .= str_pad('disabled', $colLengths['disabled'])."\t";
            } else {
                $string .= str_pad('enabled', $colLengths['disabled'])."\t";
            }
            $string .= str_pad($apiKey->getHash(), $colLengths['hash'])."\t";
            $string .= str_pad(implode(', ', $apiKey->getRoles()), $colLengths['roles']);

            if ($apiKey->getDisabled()) {
                $string = '<error>'.$string.'</error>';
            }
            $output->writeln($string);
        }
    }
}
